import React, { Component } from "react";
import "./App.css";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Learn Simple CRUD with ReactJS",
      act: 0,
      index: "",
      datas: [],
    };
  }

  componentDidMount() {
    this.refs.name.focus();
  }

  fSubmit = (e) => {
    e.preventDefault();
    console.log("try");

    let datas = this.state.datas;
    let name = this.refs.name.value;
    let code = this.refs.code.value;
    let type = this.refs.type.value;

    if (this.state.act === 0) {
      //new
      let data = {
        name,
        code,
        type,
      };
      datas.push(data);
      <Popup trigger={<button> Trigger</button>} position="right center">
        <div>Popup content here !!</div>
      </Popup>;
    } else {
      //update
      let index = this.state.index;
      datas[index].name = name;
      datas[index].code = code;
      datas[index].type = type;
    }

    this.setState({
      datas: datas,
      act: 0,
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  };

  fRemove = (i) => {
    let datas = this.state.datas;
    datas.splice(i, 1);
    this.setState({
      datas: datas,
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  };

  fEdit = (i) => {
    let data = this.state.datas[i];
    this.refs.name.value = data.name;
    this.refs.code.value = data.code;
    this.refs.type.value = data.type;

    this.setState({
      act: 1,
      index: i,
    });

    this.refs.name.focus();
  };

  render() {
    let datas = this.state.datas;
    return (
      <div className="App">
        <h2>{this.state.title}</h2>
        <form ref="myForm" className="myForm">
          <input
            type="text"
            ref="name"
            placeholder="Nama ICD 9"
            className="formField"
          />
          <input
            type="text"
            ref="code"
            placeholder="Code ICD 9"
            className="formField"
          />
          <input
            type="text"
            ref="type"
            placeholder="Type ICD 9"
            className="formField"
          />
          <button onClick={(e) => this.fSubmit(e)} className="myButton">
            submit{" "}
          </button>
        </form>
        <pre>
          {datas.map((data, i) => (
            <li key={i} className="myList">
              {i + 1}. {data.name}, {data.code}, {data.type}
              <button onClick={() => this.fRemove(i)} className="myListButton">
                remove{" "}
              </button>
              <button onClick={() => this.fEdit(i)} className="myListButton">
                edit{" "}
              </button>
            </li>
          ))}
        </pre>
      </div>
    );
  }
}

export default App;
