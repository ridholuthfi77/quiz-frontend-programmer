import React, {Component} from "react";
import {Table} from "react-bootstrap";

export class Icd9 extends Component{
    constructor(props){
        super(props);
        this.state = {icds:[]}
    }
    
    componentDidMount(){
        this.refreshList();
    }

    refreshList(){
        // fetch('https://localhost:8080/api/icd_nine_types?page=1')
        // .then(response=> response.json())
        // .then(data=> {
        //     this.setState({icds:data})
        // }
        //     )
        this.setState({
            icds : [{"icd_nines_name" : "Haha", "icd_nines_code" : "C001", "icd_nines_type" : "uhuy"}]
        })
    }

    render(){
        const {icds} = this.state;
        return(
            <Table className="mt-4" striped bordered hover size="sm">

                <thead>
                    <tr>
                        <th>Nama ICD9</th>
                        <th>Code</th>
                        <th>Tipe</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {icds.map(icd=>
                        <tr key={icd.icd_nines_code}> 
                        <td>
                            {icd.icd_nines_name}
                        </td>
                        <td>
                            {icd.icd_nines_code}
                        </td>
                        <td>
                            {icd.icd_nines_type}
                        </td>
                        <td>
                        </td>

                        </tr>
                        )}
                </tbody>

            </Table>
        )
    }

}
