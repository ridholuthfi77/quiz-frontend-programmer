import logo from './logo.svg';
import {Home} from './components/Home';
import {Icd9} from './components/Icd9';
import {Navigation} from './components/Navigation';
import './App.css';
import Button from 'react-bootstrap/Button';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className="container">
      <h3 className="m-3 d-flex justify-content-center">CRUD Sederhana</h3>
      <h5 className="m-3 d-flex justify-content-center">Sederhana Banget</h5>
      <Navigation/>
      <Switch>
        <Route path='/' component={Home} exact />
        <Route path='/Icd9' component={Icd9} exact />
      </Switch>
      
    </div>
    </BrowserRouter>
  );
}

export default App;
